package com.inim.studentsmanagementapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SignupFragment extends Fragment {

    EditText rName,rRoll,rEmail,rPass,rContact,rcPass,rAddress,rFac,rElect,rGen;
    ImageView image1,image2;
    Button sgnUp;
    TextView Lg;

    FragmentManager fm;
    FragmentTransaction ft;

    String sRoll,sName,sContact,sGender,sEmail,sPass,sAddress,sFac,sElect;


//    private static final String[] elective = new String[]{"MAD","IPSR","Optical Fiber","AI"};
//    private static final String[] gender = new String[]{"Male","Female"};

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @org.jetbrains.annotations.NotNull LayoutInflater inflater,
                             @Nullable @org.jetbrains.annotations.Nullable ViewGroup container,
                             @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.activity_signup_fragment,container,false);


        rName = v.findViewById(R.id.s_name);
        rRoll = v.findViewById(R.id.roll);
        rEmail = v.findViewById(R.id.s_email);
        rPass = v.findViewById(R.id.s_pass);
        rContact = v.findViewById(R.id.phone);
        rcPass = v.findViewById(R.id.s_cpass);
        sgnUp = v.findViewById(R.id.btn_sgnup);
        rAddress=v.findViewById(R.id.addr);
        rFac=v.findViewById(R.id.fac);

        rElect= v.findViewById(R.id.elective);
        rGen=v.findViewById(R.id.gender);

        sgnUp = v.findViewById(R.id.btn_sgnup);

//        image1 = v.findViewById(R.id.arrow1);
//        image2 = v.findViewById(R.id.arrow2);

///-------------dropdown code-----------------------------------------------------------------------
//        final AutoCompleteTextView autoCompleteTextView1;
//        final AutoCompleteTextView autoCompleteTextView2 ;
//        autoCompleteTextView1= v.findViewById(R.id.autoCmplE);
//        autoCompleteTextView2=v.findViewById(R.id.autoCmplG);
//
//        autoCompleteTextView1.setThreshold(4);//no. of list to show in dropdown.
//        autoCompleteTextView2.setThreshold(1);
//
//        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,elective);
//        autoCompleteTextView1.setAdapter(adapter1);
//
//        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,gender);
//        autoCompleteTextView2.setAdapter(adapter2);
//
//        image1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                autoCompleteTextView1.showDropDown();
//            }
//        });
//
//        image2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                autoCompleteTextView2.showDropDown();
//            }
//        });

        //-------------dropdown code ends------------------------------------------------------------//

        Lg=v.findViewById(R.id.s_login);
        Lg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fm=getParentFragmentManager();
                ft=fm.beginTransaction();
                ft.replace(R.id.view,new LoginFragment());
                ft.commit();
            }
        });
        ///-------------------Data insertion code----------------------------------------------------///

        sgnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sRoll = rRoll.getText().toString().trim();
                sName = rName.getText().toString().trim();
                sContact = rContact.getText().toString().trim();
                sEmail = rEmail.getText().toString().trim();
                sFac = rFac.getText().toString().trim();
                sGender = rGen.getText().toString().trim();
                sElect = rElect.getText().toString().trim();
                sPass= rPass.getText().toString().trim();
                sAddress= rAddress.getText().toString().trim();

                if(rName.getText().toString().isEmpty()
                        ||rContact.getText().toString().isEmpty()
                        ||rEmail.getText().toString().isEmpty()
                        ||rAddress.getText().toString().isEmpty()
                        ||rFac.getText().toString().isEmpty()
                        ||rEmail.getText().toString().isEmpty()
                        ||rPass.getText().toString().isEmpty()
                        ||rcPass.getText().toString().isEmpty()
                        ||rRoll.getText().toString().isEmpty()
                        ||rElect.getText().toString().isEmpty()
                        ||rGen.getText().toString().isEmpty())
                {
                    Toast.makeText(getContext(),"Fill all thw details",Toast.LENGTH_LONG).show();
                }
                else if(rPass.getText().toString().equals(rcPass.getText().toString()))
                {
                    StudentRepository studentRepository = new StudentRepository(getContext());
                    Student student = new Student(Integer.parseInt(sRoll),sName,sEmail,sAddress,sFac,sElect,sPass,sContact,sGender);
                    studentRepository.InsertTask(student);

                    rName.setText("");
                    rRoll.setText("");
                    rEmail.setText("");
                    rPass.setText("");
                    rContact.setText("");
                    rcPass.setText("");
                    rAddress.setText("");
                    rFac.setText("");
                    rElect.setText("");
                    rGen.setText("");

                    fm=getParentFragmentManager();
                    ft=fm.beginTransaction();
                    ft.replace(R.id.view,new LoginFragment());
                    ft.commit();
                }
                else
                    Toast.makeText(getContext(),"Password didn't mach",Toast.LENGTH_LONG).show();
            }




        });

        ///-------------------Data insertion code end----------------------------------------------------///

        return v;
    }
}