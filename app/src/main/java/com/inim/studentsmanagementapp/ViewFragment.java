package com.inim.studentsmanagementapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ViewFragment extends Fragment {
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    ArrayList<Student> studentArrayList,studentArrayList_search;

    EditText edt_search;

    CustomAdapter customAdapter;


    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @org.jetbrains.annotations.NotNull LayoutInflater inflater,
                             @Nullable @org.jetbrains.annotations.Nullable ViewGroup container,
                             @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.activity_view_fragment, container, false);


        recyclerView = v.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        edt_search = v.findViewById(R.id.edt_search);

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String text = s.toString();
                Filter(text);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        new LoadDataTask().execute();

        return v;
    }
    class LoadDataTask extends AsyncTask<Void, Void,Void> {

        StudentRepository studentRepository;
        List<Student> studentList;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            studentRepository = new StudentRepository(getContext());

        }

        @Override
        protected Void doInBackground(Void... voids) {

            studentList  = studentRepository.getStudents();
            studentArrayList = new ArrayList<>();
            studentArrayList_search = new ArrayList<>();

            for (int i=0;i<studentList.size();i++){
                studentArrayList.add(studentList.get(i));
                studentArrayList_search.add(studentList.get(i));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            customAdapter = new CustomAdapter(studentArrayList,getContext());
            recyclerView.setAdapter(customAdapter);


        }
    }

    //________Filter method Starts____________________
    public void Filter(String charText){
        charText = charText.toLowerCase(Locale.getDefault());
        Log.d("Filter",charText+"");

        studentArrayList.clear();

        if(charText.length()==0){
            studentArrayList.addAll(studentArrayList_search);
            Log.d("load data","All");
        }
        else{

            Log.d("load data","Filtered");

            for(Student student:studentArrayList_search){

                if(student.student_name.toLowerCase(Locale.getDefault()).contains(charText)
                        ||student.student_contactno.toLowerCase(Locale.getDefault()).contains(charText)
                        ||String.valueOf(student.rollno).toLowerCase(Locale.getDefault()).contains(charText)){

                    studentArrayList.add(student);
                }
            }
        }
        customAdapter.notifyDataSetChanged();
    }
    //________Filter method Ends____________________

//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        new LoadDataTask().execute();

}
