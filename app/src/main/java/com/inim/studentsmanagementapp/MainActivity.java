package com.inim.studentsmanagementapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;


public class MainActivity extends AppCompatActivity {

    FragmentTransaction ft;
    FragmentManager fm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        fm=getSupportFragmentManager();
        ft=fm.beginTransaction();
        ft.replace(R.id.view,new LoginFragment());
        ft.commit();

//        fm=getSupportFragmentManager();
//        ft=fm.beginTransaction();
//
//        viewPager=findViewById(R.id.view_pager);
//        CustomAdapter adapter = new CustomAdapter(fm,getApplicationContext());
//        viewPager.setAdapter(adapter);

    }
}