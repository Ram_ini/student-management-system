package com.inim.studentsmanagementapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.RoomDatabase;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class LoginFragment extends Fragment {

    ArrayList<Student> studentArrayList;

    EditText lEmail,lPass;


    TextView sg;
    Button lgIn;

    FragmentTransaction ft;
    FragmentManager fm;



    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @org.jetbrains.annotations.NotNull LayoutInflater inflater,
                             @Nullable @org.jetbrains.annotations.Nullable ViewGroup container,
                             @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {

        View v =inflater.inflate(R.layout.activity_login_fragment,container,false);




        lEmail=v.findViewById(R.id.email);
        lPass=v.findViewById(R.id.pass);

        sg=v.findViewById(R.id.Sign_up);
        lgIn=v.findViewById(R.id.btn_login);

        sg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fm=getParentFragmentManager();
                ft=fm.beginTransaction();
                ft.replace(R.id.view,new SignupFragment());
                ft.commit();
            }
        });

        lgIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StudentRepository studentRepository;
                studentRepository = new StudentRepository(getContext());

                String vEmail = lEmail.getText().toString().trim();
                String vPass = lPass.getText().toString().trim();

                if(vEmail.isEmpty()||vPass.isEmpty()){
                    Toast.makeText(getActivity(),"Field Empty",Toast.LENGTH_SHORT).show();
                }
                else{
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Looper.prepare();
                            List<Student> studentList;
                            studentList  = studentRepository.getStudents();
                            studentArrayList = new ArrayList<>();
                            for(Student student:studentList){
                                if(student.student_email.equals(vEmail) && student.password.equals(vPass)){
                                    fm=getParentFragmentManager();
                                    ft=fm.beginTransaction();
                                    ft.replace(R.id.view,new MenuFragment());
                                    ft.commit();

                                    Toast.makeText(getContext(),"Login Successful",Toast.LENGTH_SHORT).show();
                                }
                            }
                            return;
                        }
                    }).start();
                }
            }
        });
        return v;
    }
}
