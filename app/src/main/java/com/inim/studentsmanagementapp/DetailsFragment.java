package com.inim.studentsmanagementapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

public class DetailsFragment extends Fragment {

    EditText edt_rollno,edt_sname,edt_contact,edt_email,edt_fac,edt_addr,edt_pass,edt_cpass,edt_gender,edt_elect;
    Button btn_update,btn_delete;

    //    ImageView imageE,imageG;
    FragmentManager fm;
    FragmentTransaction ft;



//    private static final String[] elective = new String[]{"MAD","IPSR","Optical Fiber","AI"};
//    private static final String[] gender = new String[]{"Male","Female"};

    String sName="",sContact="",sGender="",sFac ="",sAddr ="",sElect="",sRoll="",sEmail="",sPass="",sCPass="";

    String sRoll_to_update="",sName_to_update="",sGender_to_update="",sContact_to_update="",sEmail_to_update="",sFac_to_update="",sElective_to_update="",sAddress_to_update="",sPass_to_update="";
    String sRoll_to_delete="",sName_to_delete="",sGender_to_delete="",sContact_to_delete="",sEmail_to_delete="",sFac_to_delete="",sElective_to_delete="",sAddress_to_delete = "",sPass_to_delete="";


    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @org.jetbrains.annotations.NotNull LayoutInflater inflater,
                             @Nullable @org.jetbrains.annotations.Nullable ViewGroup container,
                             @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {

        View v =inflater.inflate(R.layout.activity_details_fragment,container,false);


        edt_rollno = v.findViewById(R.id.u_roll);
        edt_sname = v.findViewById(R.id.u_name);
        edt_contact = v.findViewById(R.id.u_phone);
        edt_email = v.findViewById(R.id.u_email);
        edt_fac = v.findViewById(R.id.u_fac);
        edt_addr = v.findViewById(R.id.u_addr);
        edt_pass= v.findViewById(R.id.u_pass);
        edt_cpass= v.findViewById(R.id.u_cpass);

        edt_gender= v.findViewById(R.id.u_gender);
        edt_elect= v.findViewById(R.id.u_elective);

//        imageE=v.findViewById(R.id.arrow1);
//        imageG=v.findViewById(R.id.arrow2);

        btn_update = v.findViewById(R.id.btn_update);
        btn_delete = v.findViewById(R.id.btn_delete);



        //-------getting value from Custom Adapter start---------
        Bundle data = this.getArguments();
        if(data!=null){


            sRoll = data.getString("b_roll");
            sName = data.getString("b_name");
            sContact = data.getString("b_contact");
            sGender = data.getString("b_gender");
            sFac = data.getString("b_fac");
            sElect = data.getString("b_elective");
            sEmail = data.getString("b_email");
            sPass=data.getString("b_pass");
            sAddr=data.getString("b_addr");

            //Toast.makeText(getContext(),"Data received",Toast.LENGTH_LONG).show();
        }
        //-------getting value from Custom Adapter end---------

        //-------setting value from Custom Adapter to UI start---------
        edt_rollno.setText(sRoll+"");
        edt_sname.setText(sName+"");
        edt_contact.setText(sContact+"");
        edt_email.setText(sEmail+"");
        edt_fac.setText(sFac+"");
        edt_addr.setText(sAddr+"");
        edt_elect.setText(sElect+"");
        edt_gender.setText(sGender+"");
        edt_pass.setText(sPass+"");
        edt_cpass.setText(sPass+"");
        //-------setting value from Custom Adapter to UI end---------


        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt_contact.getText().toString().isEmpty()
                        ||edt_sname.getText().toString().isEmpty()
                        ||edt_fac.getText().toString().isEmpty()
                        ||edt_email.getText().toString().isEmpty()
                        ||edt_rollno.getText().toString().isEmpty()
                        ||edt_addr.getText().toString().isEmpty()
                        ||edt_elect.getText().toString().isEmpty()
                        ||edt_gender.getText().toString().isEmpty())

                {
                    Toast.makeText(getContext(),"Fill all details", Toast.LENGTH_LONG).show();
                }
                else
                {
                    sRoll_to_update=edt_rollno.getText().toString().trim();
                    sName_to_update = edt_sname.getText().toString().trim();
                    sContact_to_update=edt_contact.getText().toString().trim();
                    sEmail_to_update=edt_email.getText().toString().trim();
                    sFac_to_update=edt_fac.getText().toString().trim();
                    sAddress_to_update = edt_addr.getText().toString().trim();
                    sGender_to_update=edt_gender.getText().toString().trim();
                    sElective_to_update=edt_elect.getText().toString().trim();
                    sPass_to_update=edt_pass.getText().toString().trim();

                }

                StudentRepository studentRepository = new StudentRepository(getContext());
                //sName,sEmail,sAddress,sFac,sElect,sPass,sContact,sGender
                Student student = new Student(Integer.parseInt(sRoll_to_update),sName_to_update,sEmail_to_update,sAddress_to_update,sFac_to_update,sElective_to_update,sPass_to_update,sContact_to_update,sGender_to_update);
                studentRepository.UpdateTask(student);

                Toast.makeText(getContext(),"Values Updated",Toast.LENGTH_LONG).show();
                fm=getParentFragmentManager();
                ft=fm.beginTransaction();
                ft.replace(R.id.view,new ViewFragment());
                ft.commit();
//                getActivity().finish();
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sRoll_to_delete=edt_rollno.getText().toString().trim();
                sName_to_delete = edt_sname.getText().toString().trim();
                sContact_to_delete=edt_contact.getText().toString().trim();
                sEmail_to_delete=edt_email.getText().toString().trim();
                sFac_to_delete=edt_fac.getText().toString().trim();
                sAddress_to_delete = edt_addr.getText().toString().trim();
                sGender_to_delete=edt_gender.getText().toString().trim();
                sElective_to_delete=edt_elect.getText().toString().trim();
                sPass_to_delete=edt_pass.getText().toString().trim();

                Student student_to_delete = new Student(Integer.parseInt(sRoll_to_delete),sName_to_delete,sEmail_to_delete,sAddress_to_delete,sFac_to_delete,sElective_to_delete,sPass_to_delete,sContact_to_delete,sGender_to_delete);
                generate_delete_dialog(student_to_delete);
            }
        });

        return v;
    }

    //------------generate delete alert dialog for delete--------------
    public void generate_delete_dialog(Student student){

        final Student student_about_to_delete = student;

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),R.style.AppCompatAlertDialogStyle);///do not write getApplicationContext
        builder.setTitle("Warning");
        builder.setMessage("Do you want to Delete ?\n"+"Rollno = "+student_about_to_delete.rollno+"\n"+"Name = "+student_about_to_delete.student_name);
        builder.setIcon(android.R.drawable.ic_delete);

        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Delete code here
                StudentRepository studentRepository = new StudentRepository(getContext());
                studentRepository.Deleteask(student_about_to_delete);
                Toast.makeText(getContext(),"Values Deleted",Toast.LENGTH_LONG).show();
                fm=getParentFragmentManager();
                ft=fm.beginTransaction();
                ft.replace(R.id.view,new MenuFragment());
                ft.commit();
                //getActivity().finish();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog deleteDialog=builder.create();
        deleteDialog.show();
    }

    //------------generate delete alert dialog for delete--------------

}