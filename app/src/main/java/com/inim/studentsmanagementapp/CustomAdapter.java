package com.inim.studentsmanagementapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder>{

    FragmentManager fm;
    FragmentTransaction ft;

    private ArrayList<Student> dataset;
    Context context;

    //--------------MyView Holder start------------------------//
    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView tv_roll;
        TextView tv_name;
        TextView tv_contact;
        TextView tv_email;
        TextView tv_address;
        TextView tv_fac;
        TextView tv_elect;




        ImageView phoneImg,genImg;
        LinearLayout li_card;

        public MyViewHolder (View itemView){
            super(itemView);
            this.tv_roll = itemView.findViewById(R.id.tv_roll);
            this.tv_name = itemView.findViewById(R.id.tv_name);
            this.tv_email = itemView.findViewById(R.id.tv_email);
            this.tv_address = itemView.findViewById(R.id.tv_address);
            this.tv_fac = itemView.findViewById(R.id.tv_fac);
            this.tv_elect = itemView.findViewById(R.id.tv_elect);
            this.tv_contact = itemView.findViewById(R.id.tv_contact);
            this.phoneImg = itemView.findViewById(R.id.img_ph);
            this.genImg = itemView.findViewById(R.id.img_gen);
            this.li_card = itemView.findViewById(R.id.lin_card);
            //this.btn_title = itemView.findViewById(R.id.btn_title);
        }
    }

    //--------------------------MyView Holder End------------------------//

    public  CustomAdapter(ArrayList<Student> dataset ,Context context){
        this.dataset=dataset;
        this.context=context;

    }

    @NonNull
    @org.jetbrains.annotations.NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull @org.jetbrains.annotations.NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_students,parent,false);

        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull CustomAdapter.MyViewHolder holder, int position) {
        TextView tv_roll = holder.tv_roll;
        TextView tv_name = holder.tv_name;
        TextView tv_contact = holder.tv_contact;
        TextView tv_email = holder.tv_email;
        TextView tv_address = holder.tv_address;
        TextView tv_fac = holder.tv_fac;
        TextView tv_elect = holder.tv_elect;
        ImageView phoneImg = holder.phoneImg;
        ImageView genImg = holder.genImg;

        //Button btn_title = holder.btn_title;

        LinearLayout li_card = holder.li_card;

        tv_roll.setText(dataset.get(position).rollno+".");
        tv_name.setText(dataset.get(position).student_name+"");
        tv_contact.setText("Phone : "+dataset.get(position).student_contactno+"");
        tv_email.setText("Email : "+dataset.get(position).student_email+"");
        tv_address.setText("Address : "+dataset.get(position).student_address+"");
        tv_fac.setText("Faculty : "+dataset.get(position).student_faculty+"");
        tv_elect.setText("Elective : "+dataset.get(position).student_elective+"");


        if(dataset.get(position).student_gender.equalsIgnoreCase("male")){
            genImg.setImageResource(R.drawable.male);
        }
        else if(dataset.get(position).student_gender.equalsIgnoreCase("female")){
            genImg.setImageResource(R.drawable.female);
        }


        //--------------Logic To set Title-------------//
        // btn_title.setText(dataset.get(position).student_name.toUpperCase().charAt(0)+"");

        //--------------Random Color Logic-------------//
//        Random random = new Random();
//        int red = random.nextInt(225);
//        int green = random.nextInt(225);
//        int blue = random.nextInt(225);
//        btn_title.setBackgroundColor(Color.rgb(red,green,blue));

        if(dataset.get(position).student_contactno.length()>9){
            phoneImg.setVisibility(View.VISIBLE);
            phoneImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+dataset.get(position).student_contactno));
                    context.startActivity(intent);
                }
            });

        }else {
            phoneImg.setVisibility(View.GONE);
        }


        li_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rollno=dataset.get(position).rollno;
                String student_name = dataset.get(position).student_name;
                String contactno = dataset.get(position).student_contactno;
                String gender = dataset.get(position).student_gender;
                String address = dataset.get(position).student_address;
                String email = dataset.get(position).student_email;
                String fac = dataset.get(position).student_faculty;
                String elective = dataset.get(position).student_elective;
                String pass = dataset.get(position).password;
                //Toast.makeText(context,rollno+"\n" + student_name+"\n"+contactno+"\n"+gender,Toast.LENGTH_LONG).show();



                Bundle bundle = new Bundle();
                bundle.putString("b_name",student_name);
                bundle.putString("b_roll",rollno+"");
                bundle.putString("b_contact",contactno);
                bundle.putString("b_gender",gender);
                bundle.putString("b_email",email);
                bundle.putString("b_fac",fac);
                bundle.putString("b_addr",address);
                bundle.putString("b_elective",elective);
                bundle.putString("b_pass",pass);

                Fragment fragment = new DetailsFragment();
                fragment.setArguments(bundle);

//                Fragment fragment1= new DetailsFragment();
//                ft.add(R.id.view, fragment1);
//                ft.commit();

                //Fragment fragment1 = new DetailsFragment();
                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.view, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}

