package com.inim.studentsmanagementapp;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import androidx.room.Room;

import java.util.List;

public class StudentRepository {

    private String DB_NAME ="studentDB";

    private StudentDatabase studentDatabase;

    Context context;

    public StudentRepository(Context context) {
        this.context = context;
        studentDatabase = Room.databaseBuilder(context,StudentDatabase.class,DB_NAME).build();
    }

    //------------------Insert Task Start--------------------------//

    public void InsertTask (final Student student){
        new AsyncTask<Void,Void,Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                studentDatabase.studentDao().insertTask(student);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Toast.makeText(context, student.student_name+" is inserted",Toast.LENGTH_LONG).show();

            }
        }.execute();
    }
    //------------------Insert Task End--------------------------//

    //-----------Get Data  task starts----------------------//

    public List<Student> getStudents(){
        List<Student> studentList = studentDatabase.studentDao().getAll();
        return studentList;
    }

    //-----------Get Data  task ends----------------------//

    //----------update operation starts---------------------//

    public void UpdateTask (final Student student){
        new AsyncTask<Void,Void,Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                studentDatabase.studentDao().updateTask(student);
                return null;
            }
        }.execute();
    }

    //----------update operation ends---------------------//

    //----------Delete operation starts---------------------//

    public void Deleteask (final Student student){
        new AsyncTask<Void,Void,Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                studentDatabase.studentDao().deleteTask(student);
                return null;
            }
        }.execute();
    }

    //----------Delete operation ends---------------------//



}
