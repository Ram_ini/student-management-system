package com.inim.studentsmanagementapp;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;



@Entity
public class Student {

    @PrimaryKey
    public int rollno;

    @ColumnInfo(name="student_name")
    public String student_name;

    @ColumnInfo(name="student_address")
    public String student_address;

    @ColumnInfo(name="student_faculty")
    public String student_faculty;

    @ColumnInfo(name="student_elective")
    public String student_elective;

    @ColumnInfo(name="student_email")
    public String student_email;

    @ColumnInfo(name = "Password")
    public String password;

    @ColumnInfo (name = "student_contactno")
    public String student_contactno;

    @ColumnInfo (name ="student_gender")
    public String student_gender;



    public Student(int rollno, String student_name, String student_email, String student_address, String student_faculty, String student_elective, String password, String student_contactno, String student_gender) {
        this.rollno = rollno;
        this.student_name = student_name;
        this.student_email = student_email;
        this.student_address = student_address;
        this.student_faculty = student_faculty;
        this.student_elective = student_elective;
        this.password = password;
        this.student_contactno = student_contactno;
        this.student_gender = student_gender;
    }

}
